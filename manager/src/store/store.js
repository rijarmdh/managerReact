import { createStore, applyMiddleware } from 'redux';
import reducer from './../reducers/indexReducer';
import ReduxThunk from 'redux-thunk';

const logger = store => next => action => {
  console.log('action', action);
  next(action);
};

const middleware = applyMiddleware(ReduxThunk, logger);

const store = createStore(reducer, middleware);

export default store;
