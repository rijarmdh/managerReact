import React from 'react';
import { View } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import App from './App';
import EmployeeList from './components/EmployeeList';
import EmployeeCreate from './components/EmployeeCreate';
import EmployeeEdit from './components/EmployeeEdit';
import { Actions } from 'react-native-router-flux';

const RouterComponent = () => {
  return (
    <Router>
      <Scene key="root" hideNavBar>
        <Scene key="auth">
          <Scene key="login" component={App} title="Please Login" initial />
        </Scene>

        <Scene key="main">
          <Scene
            rightTitle="Add"
            onRight={() => {
              console.log('right@@@');
              Actions.employeecreate();
            }}
            key="employee"
            component={EmployeeList}
            title="Employee List"
            initial
          />

          <Scene
            key="employeecreate"
            component={EmployeeCreate}
            title="Add Employee"
          />

          <Scene
            key="employeeedit"
            component={EmployeeEdit}
            title="Edit Employee"
          />
        </Scene>
      </Scene>
    </Router>
  );
};

export default RouterComponent;
