import React from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';

const Spinner = props => {
  return (
    <View>
      <ActivityIndicator size="small" />
    </View>
  );
};

export { Spinner };
