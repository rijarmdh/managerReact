import React from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

const Input = props => {
  return (
    <View style={styles.inputComponent}>
      <Text style={styles.textLabel}>{props.label}</Text>

      <TextInput
        value={props.value}
        placeholder={props.placeholder}
        style={styles.input}
        secureTextEntry={props.secureTextEntry}
        underlineColorAndroid="rgba(0,0,0,0)"
        placeholderTextColor="#DBDCDB"
        onChangeText={e => props.onchange(e)}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  inputComponent: {
    flexDirection: 'row'

    // justifyContent: 'center',
  },
  textLabel: {
    marginTop: 15,
    width: 150,
    fontWeight: 'bold',
    paddingLeft: 25,
    fontSize: 16
  },
  input: {
    width: 330
  }
});

export { Input };
