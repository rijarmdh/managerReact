import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const Button = props => {
  return (
    <TouchableOpacity style={styles.button} onPress={props.press}>
      <Text style={styles.text}>{props.name}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#61A8EE',
    alignSelf: 'center',
    padding: 5,
    margin: 5,
    width: 390
  },
  text: {
    alignSelf: 'center',
    fontSize: 16
  }
});

export { Button };
