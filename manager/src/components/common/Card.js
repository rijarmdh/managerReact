import React from 'react';
import { View, StyleSheet } from 'react-native';

const Card = props => {
  return <View style={styles.card}>{props.children}</View>;
};

const styles = StyleSheet.create({
  card: {
    borderWidth: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    margin: 5,
    marginTop: 10,
    borderColor: '#DBDCDB',
    borderRadius: 4,

    shadowColor: '#29303B',
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 2 },
    elevation: 1,
    shadowOpacity: 0.2
  }
});

export { Card };
