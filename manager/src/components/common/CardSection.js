import React from 'react';
import { StyleSheet, View } from 'react-native';

const CardSection = props => {
  return <View style={styles.cardSection}>{props.children}</View>;
};

const styles = StyleSheet.create({
  cardSection: {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: '#DBDCDB',

    shadowColor: '#29303B',
    shadowRadius: 4,
    shadowOffset: { width: 0, height: 2 },
    elevation: 0,
    shadowOpacity: 0.5
  }
});

export { CardSection };
