import React from 'react';
import { View, Text, StyleSheet, Picker } from 'react-native';

const ThePicker = props => {
  return (
    <View>
      <Picker
        style={{ height: 50, width: 360, marginLeft: 20 }}
        selectedValue={props.selectedValue}
        onValueChange={e => props.onValue(e)}
      >
        <Picker.Item label="Monday" value="Monday" />
        <Picker.Item label="Tuesday" value="Tuesday" />
        <Picker.Item label="Wednesday" value="Wednesday" />
        <Picker.Item label="Thursday" value="Thursday" />
        <Picker.Item label="Friday" value="Friday" />
        <Picker.Item label="Saturday" value="Saturday" />
        <Picker.Item label="Sunday" value="Sunday" />
      </Picker>
    </View>
  );
};

export { ThePicker };
