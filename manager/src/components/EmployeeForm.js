import React from 'react';
import { View } from 'react-native';
import { CardSection, Input, ThePicker } from './common';
import { connect } from 'react-redux';
import * as actions from './../actions/employeeAction';

const EmployeeForm = props => {
  return (
    <View>
      <CardSection>
        <Input
          value={props.employ.name}
          label="Name"
          placeholder="Sarah"
          onchange={e => props.EmployeeUpdate({ prop: 'name', value: e })}
        />
      </CardSection>

      <CardSection>
        <Input
          value={props.employ.telepon}
          label="Phone"
          placeholder="555-555"
          onchange={e => props.EmployeeUpdate({ prop: 'telepon', value: e })}
        />
      </CardSection>

      <CardSection>
        <ThePicker
          selectedValue={props.employ.shift}
          onValue={e => props.EmployeeUpdate({ prop: 'shift', value: e })}
        />
      </CardSection>
    </View>
  );
};

const mapStateToProps = state => {
  return { employ: state.employee };
};

export default connect(
  mapStateToProps,
  actions
)(EmployeeForm);
