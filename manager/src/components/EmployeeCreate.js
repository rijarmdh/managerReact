import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { Card, CardSection, Button, Input, ThePicker } from './common';
import * as actions from './../actions/employeeAction';
import EmployeeForm from './EmployeeForm';

class EmployeeCreate extends React.Component {
  onButtonPress() {
    const { name, telepon, shift } = this.props.employ;

    this.props.EmployeeCreate({ name, telepon, shift: shift || 'Monday' });
  }

  render() {
    console.log(this.props);
    return (
      <Card>
        <EmployeeForm />

        <CardSection>
          <Button press={this.onButtonPress.bind(this)} name="Create" />
        </CardSection>
      </Card>
    );
  }
}
const mapStateToProps = state => {
  return {
    employ: state.employee
  };
};

export default connect(
  mapStateToProps,
  actions
)(EmployeeCreate);
