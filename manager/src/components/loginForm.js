import React from 'react';
import { Text } from 'react-native';
import firebase from 'firebase';
import { connect } from 'react-redux';
import { Card, CardSection, Input, Button, Spinner } from './common';
// import * as actions from './../actions/userAction'
import * as actions from './../actions/userAction';

class LoginForm extends React.Component {
  onRender() {
    if (this.props.loginData.error !== '') {
      return <Text>{this.props.loginData.error}</Text>;
    } else {
      if (
        this.props.loginData.create_user &&
        this.props.loginData.login_success
      ) {
        return <Text>Berhasil Tambah Data dan Login</Text>;
      } else if (
        this.props.loginData.login_success &&
        this.props.loginData.create_user === false
      ) {
        return <Text>Berhasil Login</Text>;
      }
    }
  }

  onsubmit() {
    const { email, password } = this.props.loginData; //===> mengambil nilai dari dalam objek login data : ex, email, dan password harus di passing sebagai objek ({dala kurung seperti ini})

    this.props.onLogin({ email, password }); // jika mengambil nilai dari dalam object, maka passing sebagai objek,
    //jika buka mengambil dari dalam objek, passing seperti biasa
  }

  buttonRendering() {
    if (this.props.loginData.loading) {
      return <Spinner />;
    } else {
      return <Button name="Login" press={this.onsubmit.bind(this)} />;
    }
  }

  render() {
    console.log(this.props);
    console.log(this.props.loginData.email);

    return (
      <Card>
        <CardSection>
          <Input
            value={this.props.loginData.email}
            label="Email"
            placeholder="email@gmail.com"
            onchange={this.props.emailChanged}
          />
        </CardSection>
        <CardSection>
          <Input
            value={this.props.loginData.password}
            label="Pasword"
            placeholder="123123"
            secureTextEntry
            onchange={this.props.PassChanged}
          />
        </CardSection>

        <CardSection>{this.buttonRendering()}</CardSection>
        {this.onRender()}
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginData: state.auth
  };
};

export default connect(
  mapStateToProps,
  actions
)(LoginForm);

//buat action creator
//panggil connect, masukkan fungsi action creator di dalam parameter connect ke2
// onTextChange, masukkan data ke state,
//onSubmit, this.state.name, this.state.password, pass ke action creators,
