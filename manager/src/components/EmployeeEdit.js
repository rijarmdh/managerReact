import React from 'react';
import _ from 'lodash';
import { Card, CardSection, Button } from './common';
import EmployeeForm from './EmployeeForm';
import { connect } from 'react-redux';
import * as actions from './../actions/employeeAction';

class EmployeeEdit extends React.Component {
  componentWillMount() {
    //sebelum component di render, maka data harus sudah disiapkan terlebih dahulu
    _.each(this.props.param, (value, prop) => {
      this.props.EmployeeUpdate({ prop, value }); //iterasi satu per satu kemudiam setiap iterasi dimasukkan kedalam method employee update
    });
  }

  onEdit() {
    const { name, telepon, shift, id } = this.props.employ;
    console.log({ name, telepon, shift, id });
    this.props.employeeEdit({ name, telepon, shift, id });
  }

  render() {
    console.log(this.props);
    return (
      <Card>
        <CardSection>
          <EmployeeForm />
        </CardSection>

        <CardSection>
          <Button name="Save Changes" press={this.onEdit.bind(this)} />
        </CardSection>
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return { employ: state.employee };
};

export default connect(
  mapStateToProps,
  actions
)(EmployeeEdit);
