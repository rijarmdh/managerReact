import React from 'react';
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Card } from './common';
import UsersLists from './usersLists';
import * as actions from './../actions/employeeAction';

class EmployeeList extends React.Component {
  componentWillMount() {
    this.props.EmployeeFetch();
  }

  componentWillReceiveProps(nextProps) {
    //dipanggil ketika component akan mendapatkan update props baru,
    console.log(nextProps);
  }

  render() {
    console.log(this.props);
    return (
      <Card>
        {this.props.arrayData.map(data => (
          <UsersLists key={data.id} text={data} />
        ))}
      </Card>
    );
  }
}

const mapStateToProps = state => {
  const dataEmp = state.employee.data_employee; //object tidak bisa di array map kan
  const arrayData = _.map(dataEmp, (value, id) => {
    return { ...value, id: id };
  });

  return { arrayData };
};

export default connect(
  mapStateToProps,
  actions
)(EmployeeList);
