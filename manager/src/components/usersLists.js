import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Card, CardSection } from './common';
import { Actions } from 'react-native-router-flux';

const UsersLists = props => {
  return (
    <TouchableOpacity
      onPress={() => Actions.employeeedit({ param: props.text })}
    >
      <CardSection>
        <Text style={styles.textStyle}>{props.text.name}</Text>
      </CardSection>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  textStyle: {
    fontSize: 20,
    fontWeight: 'bold',
    paddingLeft: 15
  }
});

export default UsersLists;
