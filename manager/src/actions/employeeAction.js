import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

export const EmployeeUpdate = ({ prop, value }) => {
  return {
    type: 'EMPLOYEE_UPDATE',
    payload: {
      prop,
      value
    }
  };
};

//add record database users employee
export const EmployeeCreate = ({ name, telepon, shift }) => {
  return dispatch => {
    console.log(name, telepon, shift);
    const { currentUser } = firebase.auth();
    firebase
      .database()
      .ref(`/users/${currentUser.uid}/pegawai`)
      .push({ name: name, telepon: telepon, shift: shift })
      .then(() => {
        dispatch({ type: 'FORM_RESET' }), Actions.pop();
      });
  };
};

export const EmployeeFetch = () => {
  return dispatch => {
    const { currentUser } = firebase.auth();

    firebase
      .database()
      .ref(`users/${currentUser.uid}/pegawai`)
      .on('value', snapshot => {
        dispatch({ type: 'EMPLOYEE_FETCH', payload: snapshot.val() });
      });
  };
};

export const employeeEdit = ({ name, telepon, shift, id }) => {
  return dispatch => {
    const { currentUser } = firebase.auth();

    firebase
      .database()
      .ref(`users/${currentUser.uid}/pegawai/${id}/`)
      .set({
        name: name,
        telepon: telepon,
        shift: shift
      })
      .then(res => {
        dispatch({ type: 'FORM_RESET_EDIT' });
        Actions.pop();
      })
      .catch(err => dispatch({ type: 'ERR', payload: err.message }));
  };
};
