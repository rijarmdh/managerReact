import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

export const emailChanged = data => {
  return {
    type: 'EMAIL_CHANGED',
    payload: data
  };
};

export const PassChanged = data => {
  return {
    type: 'PASS_CHANGED',
    payload: data
  };
};

export const onLogin = ({ email, password }) => {
  //argumen harus objek !!
  return dispatch => {
    dispatch({ type: 'LOADING' });

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(response => loginSuccess(dispatch, response))
      .catch(() =>
        firebase
          .auth()
          .createUserWithEmailAndPassword(email, password)
          .then(res => SignUpSuccess(dispatch, res))
          .catch(err => error(dispatch, err))
      );
  };
};

function loginSuccess(dispatch, response) {
  dispatch({ type: 'LOGIN_SUCCESS', payload: response.user });
  console.log(response.user);
  console.log('sukses login!');
  Actions.main();
}

function SignUpSuccess(dispatch, res) {
  dispatch({ type: 'CREATE_USER', payload: res.user });
  console.log('sukses tambah user !', res.user);
  Actions.main();
}

function error(dispatch, err) {
  dispatch({ type: 'ERROR', payload: err.message });
  console.log(err);
  Actions.refresh();
}
