/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Button, View } from 'react-native';
import LoginForm from './components/loginForm';
import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';

export default class App extends Component {
  componentWillMount() {
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: 'AIzaSyB_Z2rpvwT8jQ0jEm0cfMRZvZB2QVBUHMo',
        authDomain: 'manager-3c290.firebaseapp.com',
        databaseURL: 'https://manager-3c290.firebaseio.com',
        projectId: 'manager-3c290',
        storageBucket: 'manager-3c290.appspot.com',
        messagingSenderId: '300930448543'
      });
    }
  }

  render() {
    return (
      <View>
        <LoginForm />
      </View>
    );
  }
}
