import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import EmployeeReducer from './employeeReducer';

export default (reducer = combineReducers({
  auth: AuthReducer,
  employee: EmployeeReducer
}));
