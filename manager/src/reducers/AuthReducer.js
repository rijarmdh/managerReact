const AuthReducer = (
  state = {
    email: '',
    password: '',
    loading: false,
    error: '',
    login_success: false,
    create_user: false,
    user: null
  },
  action
) => {
  switch (action.type) {
    case 'EMAIL_CHANGED':
      state = { ...state, email: action.payload };
      break;
    case 'PASS_CHANGED':
      state = { ...state, password: action.payload };
      break;
    case 'LOADING':
      state = { ...state, loading: true, error: '' };
      break;
    case 'LOGIN_SUCCESS':
      state = {
        ...state,
        user: action.payload,
        error: '',
        login_success: true,
        loading: false
      };
      break;
    case 'CREATE_USER':
      state = {
        ...state,
        user: action.payload,
        error: '',
        login_success: true,
        create_user: true,
        loading: false
      };
      break;
    case 'ERROR':
      state = {
        ...state,
        error: action.payload,
        user: null,
        login_success: false,
        create_user: false,
        loading: false
      };
      break;
    default:
      break;
  }

  return state;
};
export default AuthReducer;
