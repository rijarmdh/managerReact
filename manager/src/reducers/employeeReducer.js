const INITIAL_STATE = {
  name: '',
  telepon: '',
  shift: '',
  data_employee: ''
};

export default (EmployeeReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'EMPLOYEE_UPDATE':
      state = { ...state, [action.payload.prop]: action.payload.value }; //
      //syntaks [action.payload.prop] adalah key interpolation ==> ambil nilai prop dalam reducer
      break;
    case 'EMPLOYEE_FETCH':
      state = { ...state, data_employee: action.payload };
      break;
    case 'FORM_RESET':
      state = { ...state, name: '', telepon: '', shift: '' };
      break;
    case 'FORM_RESET_EDIT':
      state = {
        ...state,
        name: '',
        telepon: '',
        shift: ''
      };
    default:
      break;
  }

  return state;
});
