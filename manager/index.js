import React from 'react';
import { AppRegistry, YellowBox } from 'react-native';

import { Provider } from 'react-redux';
import App from './src/App';
import store from './src/store/store';
import RouterComponent from './src/Router'

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader', 'Setting a timer']);

const apps = () => {
    return (
        <Provider store={store} >
            <RouterComponent/>
        </Provider>
    );
};

AppRegistry.registerComponent('manager', () => apps);
